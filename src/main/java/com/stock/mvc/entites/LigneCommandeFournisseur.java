package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class LigneCommandeFournisseur implements Serializable{
	@Id
	@GeneratedValue
		private Long LigneCommandeFournisseur;
	
	 @ManyToOne
	 @JoinColumn(name="idArticle")
	private Article article;
	 @ManyToOne
	 @JoinColumn(name="idCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;


	public Long getId() {
		return LigneCommandeFournisseur;
	}

	public void setId(Long id) {
		this.LigneCommandeFournisseur = id;
	}

	public Long getLigneCommandeFournisseur() {
		return LigneCommandeFournisseur;
	}

	public void setLigneCommandeFournisseur(Long ligneCommandeFournisseur) {
		LigneCommandeFournisseur = ligneCommandeFournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

	public LigneCommandeFournisseur() {
		
	}
	
}
