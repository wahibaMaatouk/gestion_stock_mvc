package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Client implements Serializable{
	@Id
	@GeneratedValue
		private Long Client;
	    private String nom;
	    private String prenom;
	    private String adresse;
	    private String mail;
	    private String photo ;
	    @OneToMany(mappedBy="client")
	    private List<CommandeClient> commandeClients;

	public List<CommandeClient> getCommandeClients() {
			return commandeClients;
		}

		public void setCommandeClients(List<CommandeClient> commandeClients) {
			this.commandeClients = commandeClients;
		}

	public String getPhoto() {
			return photo;
		}

		public void setPhoto(String photo) {
			this.photo = photo;
		}

	public Client() {
		}

	public Long getClient() {
			return Client;
		}

		public void setClient(Long client) {
			Client = client;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public String getAdresse() {
			return adresse;
		}

		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}

		public String getMail() {
			return mail;
		}

		public void setMail(String mail) {
			this.mail = mail;
		}

	public Long getId() {
		return Client;
	}

	public void setId(Long id) {
		this.Client = id;
	}
}
